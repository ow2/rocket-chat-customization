#!/bin/sh

USER_ID_VALUE=$1
X_AUTH_TOKEN_VALUE=$2

echo "Settings default language to English"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Language \
     -d '{"value": "en"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Add custom field to store user company"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Accounts_CustomFields \
     -d '{"value": "{\"company\": {\"type\": \"text\",\"required\": true,\"minLength\": 2}}"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Display custom field in user profile"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Accounts_CustomFieldsToShowInUserInfo \
     -d '{"value": "[{\"company\": \"company\"}]"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi


echo "Disable 2 factor authentication by email default opt in for new users"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Accounts_TwoFactorAuthentication_By_Email_Auto_Opt_In \
     -d '{"value": false}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Compact display for rooms"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Accounts_Default_User_Preferences_sidebarViewMode \
     -d '{"value": "condensed"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Sort rooms alphabetical"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Accounts_Default_User_Preferences_sidebarSortby \
     -d '{"value": "alphabetical"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Updating email template header"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Email_Header \
     -d '{"value": "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><!-- If you delete this tag, the sky will fall on your head --><meta name=\"viewport\" content=\"width=device-width\" /><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /><title>OW2con Rocket.Chat</title></head><body bgcolor=\"#F7F8FA\"><table class=\"body\" bgcolor=\"#F7F8FA\" width=\"100%\"><tr><td><!-- HEADER --><table class=\"wrap\" bgcolor=\"#F7F8FA\"><tr><td class=\"header container\"><div class=\"header-content\"><table bgcolor=\"#F7F8FA\" width=\"100%\"><tr><td><img src=\"[Site_Url_Slash]assets/logo.png\" alt=\"OW2con Rocket.chat\" width=\"150px\" /></td></tr></table></div></td></tr></table><!-- /HEADER --></td></tr><tr><td><!-- BODY --><table class=\"wrap\"><tr><td class=\"container\" bgcolor=\"#FFFFFF\"><div class=\"content\"><table><tr><td>"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Updating email template footer"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Email_Footer \
     -d '{"value": "</td></tr></table></div></td></tr></table><!-- /BODY --></td></tr><tr style=\"margin: 0; padding: 0;\"><td style=\"margin: 0; padding: 0;\"><!-- FOOTER --><table class=\"wrap\"><tr><td class=\"container\"><!-- content --><div class=\"content\"><table width=\"100%\"><tr><td align=\"center\" class=\"social\"><a href=\"https://www.ow2con.org\">OW2con webiste</a> | <a href=\"https://twitter.com/ow2\">Twitter</a> | <a href=\"https://www.linkedin.com/company/ow2-consortium\">LinkedIn</a></td></tr><tr><td align=\"center\"><h6>© OW2</h6><h6>OW2con by OW2</h6></td></tr></table></div><!-- /content --></td></tr></table><!-- /FOOTER --></td></tr></table></body></html>"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Updating file size upload limit"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/FileUpload_MaxFileSize \
     -d '{"value": 31457280}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Set Rocket.Chat instance name"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/settings/Site_Name \
     -d '{"value": "OW2Con RocketChat"}'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

echo "Rename General room to ow2con"
curl -X POST \
     -H "X-Auth-Token: $X_AUTH_TOKEN_VALUE" \
     -H "X-User-Id: $USER_ID_VALUE" \
     -H "Content-type:application/json" \
     https://rocketchat.ow2con.org/api/v1/channels.rename \
     -d '{ "roomId": "GENERAL", "name": "ow2con" }'
if [ $? -eq 0 ] 
then 
  echo "Done" 
else 
  echo "Error" >&2 
fi

# Permissions
# New role:
# - MO

# Privileges to remove for "user" role:
# Create Public Channels
# Create Private Channels
# Create Personal Access Tokens
# create-team
# Mention All
# Mention Here
# Allow file upload on mobile devices
# Start Discussion
# Start Discussion (Other-User)
# View Private Room
# 

# Privileges to remove for "moderator" role:
# add-team-channel
# add-team-member
# edit-team-channel
# edit-team-member
# remove-team-channel

# Privileges to remove for "Guest" role:
# Start Discussion
# View Direct Messages
# View Private Room

# Privileges to remove for "Anonymous" role:
# View Private Room


# Fields to Consider in Search
# remove bio to speedup search

# General → Apps
# Disable: Enable the App Framework
# Not needed

# General → NPS
# Disable: Enable NPS Survey
# don't spam users

# General → Reporting
# Disable: Send Statistics to Rocket.Chat

# General → Translations
# Update to have proper email content

# Create channels:
# help
# feedback
# say-hello

# ow2con room
# Description and Topic: The OW2con main room. View live stream and see when new discussions start. If you need help go to #help room.
# Announcement: Broadcast will be live on June 8!
# Enable: Read Only
# Add logo

# say-hello room
# Topic: Join to say hi to other OW2con participants!

# First Channel After Login: ow2con

# Layout

# Home body
# <p>Welcome to OW2con!</p>
# <p>You should be automatically member of 4 differents Rocket.Chat rooms:</p>
# <ul>
#   <li><a title="ow2con room" href="/channel/ow2con">ow2con</a>: the main room that will notify you when a new discussion dedicated to a talk is created. This room is read only.</li>
#   <li><a title="say-hello room" href="/channel/say-hello">say-hello</a>: this is the room to hangout and get in touch with other OW2con attendees</li>
#   <li><a title="help room" href="/channel/help">help</a>: go to this room if you need any assistance with OW2con platform</li>
#   <li><a title="feedback room" href="/channel/feedback">feedback</a>: we would love to hear your thoughts about this edition of OW2con!</li>
# </ul>


# Terms of service
# <a title="Privacy policy" href="https://www.ow2.org/view/Privacy_Policy/">Privacy policy</a>

# Login Terms
# By proceeding you are agreeing to our <a href="https://www.ow2.org/view/Privacy_Policy/">Privacy Policy</a>.

# Privacy Plocy
# By proceeding you are agreeing to our <a href="https://www.ow2.org/view/Privacy_Policy/">Privacy Policy</a>.

# Legal Notice
# This website is licensed under a Creative Commons 4.0 license


# Enable: Allow Special Characters in Room Names

# Enable Livestream Enabled

# Disable: Embed Link Previews

# OAuth 
# GitHUb
# Client secret d1002f241ac65d7c75bd3014d6e93d301266abef
# Client Id 02e26b14f9fb48208236
# See https://github.com/settings/applications/1283968 account ammottier

# Disable omnichannel


# Email → SMTP
# Host: smtp.ow2.org
# From Email: rocketchat@ow2con.org

# LiveStream & Broadcasting
# Enable: Livestream Enabled

# OTR
# Disable

# Setup Wizard
# Allow Marketing Emails: disable