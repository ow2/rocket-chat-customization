console.log('Loading of custom login script ');
function retrieveFP() {
  var checkFP = setInterval(function() {
    console.info('checkFP called');
    var forgotPassword = document.getElementsByClassName('forgot-password');
    if (forgotPassword.length) {
      // Stop periodic check for element
      clearInterval(checkFP);
      console.info('FP found');
      var element = document.createElement('div');
      element.innerHTML='<span class="warn">Please DO NOT use your OW2 account, create a new account instead</span>';
      var form = document.getElementById("login-card");
      form.insertBefore(element, form.firstChild);
    }
  }, 250);
}
retrieveFP();