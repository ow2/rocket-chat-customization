console.log('Loading of custom script');

function startVideo(bannerElements) {
  // List of element should only include one
  var banner = bannerElements[0];

  // The mouse click event we want to send to simulate a click on the banner
  var event = new MouseEvent('click', {
    'view': window,
    'bubbles': true,
    'cancelable': true
  });

  // We send the click
  var canceled = !banner.dispatchEvent(event);

  if (canceled) {
    //Un gestionnaire appelé preventDefault. 
    console.log("canceled");
  } else {
    //Aucun gestionnaires appelé preventDefault.
    console.log("not canceled");

    // We want to prevent the user from using the banner to launch the video.
    // We want to avoid that because the video is not located where we want in the DOM.
    // So we remove the banner from the DOM.
    //banner.remove();
  }
}

function autostartVideo() {

  // Check periodically that we have loaded the OW2con channel page
  var checkWindowOW2conChannel = setInterval(function () {
    if (window.location.pathname === "/channel/ow2con") {
      console.log("We are on /channel/ow2con, stop looping");
      
      // Stop periodic check for OW2con channel page
      clearInterval(checkWindowOW2conChannel);
      
      // Check periodically that banner to start the video is available
      var checkExist = setInterval(function () {

        console.log("Periodic check for banner element.");

        // Get the banner element using class name (get a list)
        var annoucement = document.getElementsByClassName('rcx-css-anh0t5');

        // If we have at least one element it means that it is loaded
        if (annoucement.length) {
          console.log("Found a banner element");

          // Stop periodic check for element
          clearInterval(checkExist);

          // Start the video
          startVideo(annoucement);

        }
      }, 100);
    } else {
      console.log("Not the correct window: " + window.location.pathname);
    }
  }, 100);
}

console.log("Start of custom script");
autostartVideo();
